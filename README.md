# Be Relevant
A nifty Craft CMS dashboard widget that tells you about potentially outdated entries.

You can add multiple versions of the widget on your dashboard and set:-

- A custom widget title, (eg: "Legal pages that need review")
- The section
- How many days since updating should it be considered old
- The limit of entries that can display in the widget

## Current Shortcomings
- It doesn't take into account locales
- It doesn't take into account user permissions
- It doesn't work with singles (might work if choose "All Sections")
- It doesn't work with globals

## Support
Fire us a tweet @ryeagency, we'll try to help but this isn't really a supported product.

## License
Do with it whatever you want. Have fun.
